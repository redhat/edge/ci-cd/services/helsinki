CS8: str = "cs8"
COMMON: str = "common"
ARCH: str = "arch"
AARCH64: str = "aarch64"
X86_64: str = "x86_64"
schema = list[dict[dict[dict[str, list]]]]


class PackageManifestSchema:
    def __init__(self, package_manifest_obj: schema):
        self.common: dict[str, list] = package_manifest_obj[CS8][COMMON]
        self.aarch64: dict[str, list] = package_manifest_obj[CS8][ARCH][
            AARCH64
        ]
        self.x86_64: dict[str, list] = package_manifest_obj[CS8][ARCH][X86_64]
