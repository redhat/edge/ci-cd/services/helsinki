import json
import re
import sys
from typing import Optional

import pkg_manifest_schema
import requests


def nvr(nvr: str) -> Optional[str]:
    pattern_nvr = re.compile(
        r"^([a-zA-Z0-9_\-\+]*)-([a-zA-Z0-9_\.]*)-([a-zA-Z0-9_\.]*)"
    )
    result = ()
    try:
        result = pattern_nvr.search(nvr).groups()
    except (AttributeError, ValueError) as e:
        print(
            f"Error {e} in searching the pattern {pattern_nvr} for nvr {nvr}"
        )
        return

    return result[0]  # return only name from nvr


def get_upstream_manifest(name: str) -> str:
    try:
        response = requests.get(name, allow_redirects=True)
    except requests.exceptions.RequestException as e:
        raise SystemExit(e)
    response = json.loads(response.text)
    return response


def main() -> None:
    """main function"""
    file_data: pkg_manifest_schema.schema = get_upstream_manifest(
        "https://gitlab.com/redhat/automotive/automotive-sig/-/raw/"
        "main/package_list/cs8-image-manifest.json"
    )

    if file_data:
        pkg_obj: pkg_manifest_schema.PackageManifestSchema = (
            pkg_manifest_schema.PackageManifestSchema(file_data)
        )
    else:
        print("Trouble in getting file data, exiting the program!")
        sys.exit(1)

    for k, v in vars(pkg_obj).items():
        for pkg in v:
            pkg_name = nvr(pkg)
            if pkg_name:
                print(pkg_name)


if __name__ == "__main__":
    main()
