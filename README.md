# helsinki

*helsinki* is a service that monitors builds of packages from *cs8 (CentOS Stream 8)*. Then, it publishes those build messages to the message broker [RabbitMQ](https://www.rabbitmq.com/).

# Getting Started

*helsinki* scrapes the builds from [CentOS](https://koji.mbox.centos.org/koji/), formulates the build to message format, and publishes those messages to the message broker *RabbitMQ*. The message queue is being listened to by another service called [rome](https://gitlab.com/redhat/edge/ci-cd/a-team/rome). 

## Installation
### Using container

**Prerequisite**

- Install container - software packager. You can use any container of your choice ([docker](https://www.docker.com/) or [podman](https://podman.io/)) to build the service image and get it up and running. All the necessary rules are already defined in the *Makefile*. 
- Pull and run RabbitMQ containerized service. Follow the [documentation](https://www.rabbitmq.com/download.html).

**To start *helsinki* in the development environment**

- Navigate to the project root directory and run `make dev/up`. This command will create the image called *helsinki* and start the container.

### Without container

**Prerequisite**
- Install RabbitMQ and start the service. Follow the [documentation](https://www.rabbitmq.com/download.html).
- Download and install [python](https://www.python.org/) version 3.6 or above
- Install [pip](https://pypi.org/project/pip/)
- Navigate to project directory and install dependencies: ` pip install -r requirements.txt`

**To start *helsinki* in the development environment**

- Navigate to project root directory and run `./helsinki/main.py`

# How to contribute

- Fork the project
- Clone your fork
- Add upstream remote: `git remote add upstream https://gitlab.com/redhat/edge/ci-cd/a-team/helsinki.git`
- Checkout new branch from *main* branch: `git checkout -b <branch-name>`
- Commit and push your branch
```
git add <file-name>
git commit -m <commit-messages>
git push origin <branch-name>
```
- Create a merge request against the upstream repository and provide a summary of what it is about
- Assign reviewers
- Resolve reviews if any
- Wait for approval(s) and your branch will be merged
### Code guidelines

- Provide unit test(s)
- For code format, *isort*, *flake8* and *black* are applied.
- Before pushing your code, make sure that linter and unit tests pass.
```
#Inside the project root directory, activate the virtual environment and run the following commands:

source <venv-name>/bin/activate
make lint
make unit
```

# Report issues

If you find any issue/bug, please don't hesitate to create one via this [link](https://gitlab.com/redhat/edge/ci-cd/a-team/helsinki/-/issues/new).
