# Deployment

## Installation

- Install the required modules listed in the `deployment/requirements.txt`
```
pip install -r deployment/requirements.txt
```
- Download and install [OpenShift CLI](https://docs.openshift.com/container-platform/4.8/cli_reference/openshift_cli/getting-started-cli.html)

## Prerequisite

- Login to the cluster
  - Using the `oc login` command retrieved from the openshift cluster
## Auto deployment

Auto deployment automates the process of manually building and deploying the image to the cluster. The defined rules will take care of triggering the deployment process for every merge event on the **main** branch. This is as well done by integrating with [GitLab Webhook](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html)

### To push the deployment script

**Prerequisite**

The following values must be provided in either `/deployment/vars/stage.yml` or `/deployment/vars/prod.yml` based on the environment.

```
ocp_ns: #namespace into which k8s resources will be deployed
validate_certs: #value of yes/no
ocp_host: #url of the OpenShift host
ocp_key: #API access token to the openshift host
```

Navigate to the project root directory:

#### Start staging environment

```
make stage/up
```

#### Start production environment

```
make prod/up
```
