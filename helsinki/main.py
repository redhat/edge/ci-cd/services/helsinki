#!/usr/bin/env python3
import logging
import logging.config
import os
import sys

import click

import publisher
import web_scraper


@click.command()
@click.option("-h", "--msg_broker_host", is_flag=True)
@click.argument("name", type=click.STRING, required=False)
def main(msg_broker_host: str, name: str) -> None:
    """main function"""

    logging.config.fileConfig(
        os.path.join(
            os.path.dirname(os.path.realpath(__file__)), "logging.conf"
        )
    )
    logger = logging.getLogger("c8s-build-update-notifier")
    logger.info("::Service started::\n")
    try:
        if msg_broker_host:
            obj_publisher = publisher.Publisher(name)
        obj_publisher = publisher.Publisher()
        scraper = web_scraper.WebScraper("", obj_publisher)
        while True:
            scraper.execute()
    except ValueError:
        logger.error(
            f"c8s-build-update-notifier exception: {ValueError} occured, \
                please restart!"
        )


if __name__ == "__main__":
    main(sys.argv[1:])
